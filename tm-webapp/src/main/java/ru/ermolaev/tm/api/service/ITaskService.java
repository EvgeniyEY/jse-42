package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.User;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task getOneById(@Nullable String id) throws Exception;

    @Nullable
    Task createTask(@Nullable User user, @Nullable TaskDTO taskDTO) throws Exception;

    @Nullable
    Task updateById(@Nullable User user, @Nullable TaskDTO taskDTO) throws Exception;

    void updateStartDate(@Nullable User user, @Nullable TaskDTO taskDTO) throws Exception;

    void updateCompleteDate(@Nullable User user, @Nullable TaskDTO taskDTO) throws Exception;

    @NotNull
    Long countAllTasks();

    @NotNull
    Long countByUserId(@Nullable User user) throws Exception;

    @NotNull
    Long countByProjectId(@Nullable String projectId) throws Exception;

    @NotNull
    Long countByUserIdAndProjectId(@Nullable User user, @Nullable String projectId) throws Exception;

    @Nullable
    Task findOneById(@Nullable String id) throws Exception;

    @Nullable
    Task findOneById(@Nullable User user, @Nullable String id) throws Exception;

    @Nullable
    Task findOneByName(@Nullable User user, @Nullable String name) throws Exception;

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAllByUserId(@Nullable User user) throws Exception;

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String projectId) throws Exception;

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@Nullable User user, @Nullable String projectId) throws Exception;

    void removeOneById(@Nullable String id) throws Exception;

    void removeOneById(@Nullable User user, @Nullable String id) throws Exception;

    void removeOneByName(@Nullable User user, @Nullable String name) throws Exception;

    void removeAll();

    void removeAllByUserId(@Nullable User user) throws Exception;

    void removeAllByProjectId(@Nullable String projectId) throws Exception;

    void removeAllByUserIdAndProjectId(@Nullable User user, @Nullable String projectId) throws Exception;

}

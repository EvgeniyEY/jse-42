package ru.ermolaev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class TaskSoapEndpoint {

    @Autowired
    private ITaskService taskService;

    @Nullable
    @WebMethod
    public TaskDTO createTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO
    ) throws Exception {
        return TaskDTO.toDTO(taskService.createTask(UserUtil.getAuthUser(), taskDTO));
    }

    @Nullable
    @WebMethod
    public TaskDTO updateTaskById(
            @WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO
    ) throws Exception {
        return TaskDTO.toDTO(taskService.updateById(UserUtil.getAuthUser(), taskDTO));
    }

    @NotNull
    @WebMethod
    public Long countAllTasks() throws Exception {
        return taskService.countByUserId(UserUtil.getAuthUser());
    }

    @Nullable
    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        return TaskDTO.toDTO(taskService.findOneById(UserUtil.getAuthUser(), id));
    }

    @NotNull
    @WebMethod
    public List<TaskDTO> findAllTasks() throws Exception {
        return taskService.findAllByUserId(UserUtil.getAuthUser());
    }

    @WebMethod
    public void removeTaskById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        taskService.removeOneById(UserUtil.getAuthUser(), id);
    }

}

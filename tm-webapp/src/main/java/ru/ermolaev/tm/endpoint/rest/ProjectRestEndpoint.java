package ru.ermolaev.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping(value = "/api/rest/project", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectRestEndpoint {

    private final IProjectService projectService;

    @Autowired
    public ProjectRestEndpoint(
            @NotNull final IProjectService projectService
    ) {
        this.projectService = projectService;
    }

    @PostMapping(value = "/create")
    public ProjectDTO createProject(
            @RequestBody @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.createProject(UserUtil.getAuthUser(), projectDTO));
    }

    @PutMapping(value = "/updateById")
    public ProjectDTO updateById(
            @RequestBody @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.updateById(UserUtil.getAuthUser(), projectDTO));
    }

    @NotNull
    @GetMapping(value = "/countAll")
    public Long countAllProjects() throws Exception {
        return projectService.countUserProjects(UserUtil.getAuthUser());
    }

    @Nullable
    @GetMapping(value = "/findById/{id}")
    public ProjectDTO findById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.findOneById(UserUtil.getAuthUser(), id));
    }

    @NotNull
    @GetMapping(value = "/findAll")
    public List<ProjectDTO> findAll() throws Exception {
        return projectService.findAllByUserId(UserUtil.getAuthUser());
    }

    @DeleteMapping(value = "/removeById/{id}")
    public void removeById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception {
        projectService.removeOneById(UserUtil.getAuthUser(), id);
    }

}

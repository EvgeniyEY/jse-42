package ru.ermolaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.endpoint.soap.TaskDTO;
import ru.ermolaev.tm.endpoint.soap.TaskSoapEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.service.SessionService;

import java.util.List;

@Component
public class TaskListShowListener extends AbstractTaskListener {

    @Autowired
    public TaskListShowListener(
            @NotNull final TaskSoapEndpoint taskEndpoint,
            @NotNull final SessionService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    @EventListener(condition = "@taskListShowListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[TASK LIST]");
        sessionService.setListCookieRowRequest(taskEndpoint);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.findAllTasks();
        if (tasks == null) return;
        for (@NotNull final TaskDTO task : tasks) {
            System.out.println(new StringBuilder()
                    .append((tasks.indexOf(task) + 1))
                    .append(". {id: ")
                    .append(task.getId())
                    .append("; name: ")
                    .append(task.getName())
                    .append("; description: ")
                    .append(task.getDescription())
                    .append("}"));
        }
        System.out.println("[COMPLETE]");
    }

}

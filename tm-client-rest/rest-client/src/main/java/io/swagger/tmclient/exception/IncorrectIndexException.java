package io.swagger.tmclient.exception;

import org.jetbrains.annotations.NotNull;

public class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException() {
        super("Error! Index is incorrect.");
    }

    public IncorrectIndexException(@NotNull final String value) {
        super("Error! This value [" + value + "] is not number.");
    }

}

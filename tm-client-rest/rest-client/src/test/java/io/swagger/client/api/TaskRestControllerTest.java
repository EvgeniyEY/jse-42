package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.ProjectDTO;
import io.swagger.client.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

public class TaskRestControllerTest {

    private final DefaultApi api = new DefaultApi();

    @Test
    public void integrationTaskTest() throws ApiException {
        @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId("e4b137a5-e090-4c42-b68d-cbfc5350e0fe");
        projectDTO.setName("rest-test-project-name");
        projectDTO.setDescription("rest-test-project-description");
        @NotNull final ProjectDTO createdProject = api.createProject(projectDTO);

        final int startValueCount = api.countAllTasks().intValue();
        final int startValueFindAll = api.findAll_0().size();

        @NotNull TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("rest-test-task-name");
        taskDTO.setDescription("rest-test-task-description");
        taskDTO.setUserId("e4b137a5-e090-4c42-b68d-cbfc5350e0fe");
        taskDTO.setProjectId(createdProject.getId());
        @NotNull final TaskDTO createdTask = api.createTask(taskDTO);

        Assert.assertEquals(startValueCount + 1, api.countAllTasks().intValue());
        Assert.assertEquals(startValueFindAll + 1, api.findAll_0().size());

        @NotNull final String newName = "UPDATED-rest-test-task-name";
        @NotNull final String newDescription = "UPDATED-rest-test-task-description";

        createdTask.setName(newName);
        createdTask.setDescription(newDescription);
        api.updateById_0(createdTask);

        @NotNull final TaskDTO findTask = api.findById_0(createdTask.getId());
        Assert.assertNotNull(findTask);
        Assert.assertEquals(findTask.getName(), newName);
        Assert.assertEquals(findTask.getDescription(), newDescription);

        api.removeOneById(createdTask.getId());
        Assert.assertEquals(startValueCount, api.countAllTasks().intValue());
        Assert.assertEquals(startValueFindAll, api.findAll_0().size());

        api.removeById(createdProject.getId());
    }

}

/*
 * Swagger Maven Plugin Sample
 * This is a sample for swagger-maven-plugin
 *
 * OpenAPI spec version: v1
 * Contact: ermolaev@yandex.ru
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.ProjectDTO;
import io.swagger.client.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import org.junit.Ignore;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for DefaultApi
 */
public class DefaultApiTest {

    private final DefaultApi api = new DefaultApi();

//    private String session;

//    @Test
//    public void countAllProjectsTest1() throws IOException {
//        @NotNull final URL url = new URL("http://localhost:8080/api/rest/authentication/login?username=admin&password=admin");
//        @NotNull final URLConnection connection = url.openConnection();
//        session = connection.getHeaderField("Set-Cookie");
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void countAllProjectsTest() throws ApiException {
//        Long response = api.countAllProjects();
//        System.out.println(response);
//         TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void countAllTasksTest() throws ApiException {
//        Long response = api.countAllTasks();
//
//         TODO: test validations
//    }

//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void createProjectTest() throws ApiException {
//        ProjectDTO body = null;
//        ProjectDTO response = api.createProject(body);
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void createTaskTest() throws ApiException {
//        TaskDTO body = null;
//        TaskDTO response = api.createTask(body);
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void findAllTest() throws ApiException {
//        List<ProjectDTO> response = api.findAll();
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void findAll_0Test() throws ApiException {
//        List<TaskDTO> response = api.findAll_0();
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void findByIdTest() throws ApiException {
//        String id = null;
//        ProjectDTO response = api.findById(id);
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void findById_0Test() throws ApiException {
//        String id = null;
//        TaskDTO response = api.findById_0(id);
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void loginTest() throws ApiException {
//        String username = "admin";
//        String password = "admin";
//        Boolean response = api.login(username, password);
//        System.out.println(response);
//
//        Long response1 = api.countAllProjects();
//        System.out.println(response1);
//    }


    @Test
    public void test1() throws ApiException {
        Long response1 = api.countAllProjects();
        System.out.println(response1);
    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void logoutTest() throws ApiException {
//        api.logout();
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void removeByIdTest() throws ApiException {
//        String id = null;
//        api.removeById(id);
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void removeOneByIdTest() throws ApiException {
//        String id = null;
//        api.removeOneById(id);
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void updateByIdTest() throws ApiException {
//        ProjectDTO body = null;
//        ProjectDTO response = api.updateById(body);
//
//        // TODO: test validations
//    }
//
//    /**
//     *
//     *
//     *
//     *
//     * @throws ApiException
//     *          if the Api call fails
//     */
//    @Test
//    public void updateById_0Test() throws ApiException {
//        TaskDTO body = null;
//        TaskDTO response = api.updateById_0(body);
//
//        // TODO: test validations
//    }
    
}


# Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**startDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**completeDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**creationDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
**tasks** | [**List&lt;Task&gt;**](Task.md) |  |  [optional]



